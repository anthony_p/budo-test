var React = require('react')
var ReactPivot = require('react-pivot')
var createReactClass = require('create-react-class')
var rows = require('./data.json')

var dimensions = [
  {value: 'host', title: 'Host'},
  {value: 'date', title: 'Date'},
  {value: 'strategy', title: 'Stratage'}
]
var reduce = function(row, memo) {
  if(row.type == "impression"){
    memo.impressions = (memo.impressions || 0) + 1
  }
  if(row.type == "load"){
    memo.loads = (memo.loads || 0) + 1;
  }
  if(row.type == "display"){
    memo.display =  (memo.display || 0) + 1 
  }
  return memo
}

var calculations = [
  {
    title: "Ipmression",
    value: function(memo) {return memo.impressions}
  },
  {
    title: "Loads",
    value: function(memo) {return memo.loads}
  },
  {
    title: "Display",
    value: function(memo){return memo.display}
  },
  { 
    title: "Load rate",
    value: function(memo){ return memo.loads / memo.impressions},
    template: function(val, row)  { return val ? val.toFixed(2) : 'n/a' }
  },
  {
    title: "Display rate",
    value: function(memo){ return memo.display / memo.loads},
    template: function(val, row) { return val ? val.toFixed(2) : 'n/a' }
  }


]

module.exports = createReactClass({

  render () {
    return <div>
      <ReactPivot rows={rows}
              dimensions={dimensions}
              reduce={reduce}
              calculations={calculations} 
              activeDimensions={['Host', 'Date']}
              />,
    </div>
  }
})
